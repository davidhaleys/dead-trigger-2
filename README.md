# Dead Trigger 2

[Dead Trigger 2 mod apk](https://apkdudes.com/dead-trigger-2-mod-apk/) is an action game combined with unique survival elements using the fascinating and beautiful Zombie subject. The player’s mission will be to become the lucky ones to survive in the middle of a pandemic; the player will have to find ways to survive and fight and support different survivors together.
